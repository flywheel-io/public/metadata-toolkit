import pytest
from pydicom.datadict import get_private_entry, get_entry, keyword_dict
from flywheel_metadata.file.dicom import (
    extend_private_dictionaries,
    extend_public_dictionary,
)


@pytest.fixture(scope="function")
def extend_public_dictionary_fixture():
    extend_public_dictionary()


@pytest.mark.parametrize(
    "hex_key,keyword",
    [
        (0x00400248, "PerformedStationNameCodeSequence"),
        (0x00381234, "ReferencedPatientAliasSequence"),
    ],
)
def test_pydicom_public_dictionary_raises_without_extension(hex_key, keyword):
    with pytest.raises(KeyError):
        de = get_entry(hex_key)
        assert de
        assert keyword in keyword_dict


@pytest.mark.parametrize(
    "hex_key,keyword",
    [
        (0x00400248, "PerformedStationNameCodeSequence"),
        (0x00381234, "ReferencedPatientAliasSequence"),
    ],
)
def test_pydicom_public_dictionary_is_extended(
    extend_public_dictionary_fixture, hex_key, keyword
):
    de = get_entry(hex_key)
    assert de
    assert keyword in keyword_dict


def test_pydicom_private_dictionary_is_extended():
    # Only testing one
    with pytest.raises(KeyError):
        get_private_entry(0x00411001, "SIEMENS MI RWVM SUV")

    extend_private_dictionaries()
    de = get_private_entry(0x00411001, "SIEMENS MI RWVM SUV")
    assert de
