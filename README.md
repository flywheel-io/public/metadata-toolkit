# metadata-toolkit

A toolkit for extracting and editing metadata from files and Flywheel objects.

## Installation
```
pip install flywheel-metadata-toolkit
```

## Contributing

### Building

It's recommended that you use [pipenv](https://docs.pipenv.org/en/latest/) to manage dependencies. For example:
```
> python3 -m pip install pipenv
> pipenv install -e .[dev] --pre
```

### Testing

Tests and python linting can be done using the `tests.sh` script, after running pipenv install:
```
> pipenv run tests/bin/tests.sh
```
